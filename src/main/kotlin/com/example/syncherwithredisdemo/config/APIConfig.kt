package com.example.syncherwithredisdemo.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component

@Component
@PropertySource("classpath:application.properties")
class APIConfig {
    @Value("\${deepsearch.api.url}")
    lateinit var DEEPSEARCH_API_URL: String
}