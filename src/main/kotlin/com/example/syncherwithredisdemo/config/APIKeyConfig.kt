package com.example.syncherwithredisdemo.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.PropertySource
import org.springframework.stereotype.Component

@Component
@PropertySource("classpath:api-key.properties")
class APIKeyConfig {
    @Value("\${deepsearch.api.key}")
    lateinit var DEEPSEARCH_API_KEY: String
}