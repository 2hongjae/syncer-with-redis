package com.example.syncherwithredisdemo.controller

import com.example.syncherwithredisdemo.service.TrendingTopicService
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/sync/haystack/v1")
class SyncController (
    val trendingTopicService: TrendingTopicService
        ){

    @GetMapping("topics/trending/news")
    fun getTrendingTopics(): ResponseEntity<Any> {
        return ResponseEntity.ok(trendingTopicService.getTrendingTopicRedis())
    }

    @PutMapping("topics/trending/news")
    fun putTrendingTopics(): ResponseEntity<Any> {
        return ResponseEntity.ok(trendingTopicService.updateTrendingTopic())
    }
}