package com.example.syncherwithredisdemo.controller

import com.example.syncherwithredisdemo.service.SearchService
import com.example.syncherwithredisdemo.service.TrendingTopicService
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/deepsearch/haystack/v1")
class DeepsearchController (
    val trendingTopicService: TrendingTopicService,
    val searchService: SearchService
    ) {

    @Value("\${deepsearch.api.key}")
    lateinit var deepsearchAPIKey: String

    /*
    1. Search(다양한 검색어에 따라 문서를 검색)
       - /haystack/v1/<string:category>/_search[?arg=value, ...]
       - /haystack/v1/<string:category>/<string:section>/_search[?arg=value, ...]
     */
    @GetMapping(value = ["{category}/_search","{category}/{section}/_search"])
    fun getSearch(@PathVariable(value = "category") category: String,
                  @PathVariable(value = "section", required = false) section: String?
    ): ResponseEntity<Any> {
        return ResponseEntity.ok(searchService.getSearch(category, section))

    }

    /*
    2. Trending Topics(현재 시장에서 이슈가 되고 있는 주제 및 관련 문서를 추출하여 제공)
       - /haystack/v1/topics/trending/news
     */
    @GetMapping("topics/trending/news")
    fun getTrendingTopics(): ResponseEntity<Any> {
        return ResponseEntity.ok(trendingTopicService.getTrendingTopic())
    }
}