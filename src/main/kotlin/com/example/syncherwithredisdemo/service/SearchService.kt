package com.example.syncherwithredisdemo.service

import com.example.syncherwithredisdemo.config.APIConfig
import com.example.syncherwithredisdemo.config.APIKeyConfig
import com.example.syncherwithredisdemo.domain.TrendingTopicRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Service
class SearchService(val trendingTopicRepository: TrendingTopicRepository) {

    @Autowired
    lateinit var apiKeyConfig: APIKeyConfig

    @Autowired
    lateinit var apiConfig: APIConfig

    // From Deepsearch API
    fun getSearch(category: String, section: String?): String? {
        val client = HttpClient.newBuilder().build()
        val requestUri = if (section.isNullOrEmpty()) {
            category
        } else {
            "$category/$section"
        }

        val request = HttpRequest.newBuilder()
                // https://api.ddi.deepsearch.com/ properties
            .uri(URI.create("${apiConfig.DEEPSEARCH_API_URL}/$requestUri/_search?query=*"))
            .header("Authorization", apiKeyConfig.DEEPSEARCH_API_KEY) //static으로 변경
            .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        return response.body()
    }
}