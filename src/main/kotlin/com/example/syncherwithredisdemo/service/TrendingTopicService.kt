package com.example.syncherwithredisdemo.service

import com.example.syncherwithredisdemo.config.APIConfig
import com.example.syncherwithredisdemo.config.APIKeyConfig
import com.example.syncherwithredisdemo.domain.TrendingTopic
import com.example.syncherwithredisdemo.domain.TrendingTopicRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.net.URI
import java.net.http.HttpClient
import java.net.http.HttpRequest
import java.net.http.HttpResponse

@Service
class TrendingTopicService(val trendingTopicRepository: TrendingTopicRepository) {

    @Autowired
    lateinit var apiConfig: APIConfig

    @Autowired
    lateinit var apiKeyConfig: APIKeyConfig


    // From Deepsearch API
    fun getTrendingTopic(): String? {
        val client = HttpClient.newBuilder().build()

        val request = HttpRequest.newBuilder()
                //properties 로 이동
            .uri(URI.create("${apiConfig.DEEPSEARCH_API_URL}/haystack/v1/topics/trending/news"))
            .header("Authorization", apiKeyConfig.DEEPSEARCH_API_KEY) // static으로 선언
            .build()

        val response = client.send(request, HttpResponse.BodyHandlers.ofString())

        return response.body()
    }

    // From Redis DB
    fun getTrendingTopicRedis(): String? {
        val data = trendingTopicRepository.findById(1.toString())
        return data.toString()
    }

    fun updateTrendingTopic(): TrendingTopic {
        // id-> static 으로 변경
        val trendingTopic = TrendingTopic(1, getTrendingTopic())
        return trendingTopicRepository.save(trendingTopic)
    }
}