package com.example.syncherwithredisdemo.domain

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface TrendingTopicRepository: CrudRepository<TrendingTopic, String> {
}