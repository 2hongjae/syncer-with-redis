package com.example.syncherwithredisdemo.domain

import org.springframework.data.annotation.Id
import org.springframework.data.redis.core.RedisHash

@RedisHash("TrendingTopic")
data class TrendingTopic(
    @Id
    val id: Long,
    val data: String? = null
)
