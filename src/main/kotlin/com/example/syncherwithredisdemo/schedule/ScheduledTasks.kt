package com.example.syncherwithredisdemo.schedule

import com.example.syncherwithredisdemo.service.TrendingTopicService
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class ScheduledTasks(
    val trendingTopicService: TrendingTopicService
) {

    @Scheduled(fixedRate = 10000)
    fun reportCurrentTime() {
        trendingTopicService.updateTrendingTopic()
    }
}