## MyData Synchronizer
### 프로젝트 코드명
- Syncer

### 목표
- 현재 클라이언트에서 외부 Deepsearch API를 직접 호출하는 방식에서 자체 내부 Syncer API를 호출하는 방식으로 변경하여 API 응답시간을 감소시키고, Deepsearch 서버 장애 시 내부 Redis 최신 적재 정보로 대응 가능하도록 한다.

### 역할
- Deepsearch API 주기적 요청
- Deepsearch API 응답내용 Redis 적재
- Redis에 적재된 내용 클라이언트 요청 시 전달

### 참고
- Deepsearch Guide : https://help.deepsearch.com/ddi/ 
- Wiki : https://koreaivplatformdev.atlassian.net/wiki/spaces/serverdev/pages/184484146
- API prefix URL(임시) : http://49.50.174.244:8083/
    - example : http://49.50.174.244:8083/deepsearch/trending-topics 
